module SeedCandidate
  def self.seed
    grades.each do |candidate_name, candidate_infos|
      candidate = AssistantCandidate.find_or_initialize_by(name: candidate_name, npm: candidate_infos[0])
      candidate_infos[1].each do |criteria_name, grade|
        criterium = Criterium.find_by(name: criteria_name)
        grade_criteria = candidate.candidate_grades.find_or_initialize_by(criterium_id: criterium.id)
        grade_criteria.grade = grade
      end
      candidate.save
    end
  end

  def self.grades
    @grades ||=
    {
      'Calas 1'=> ['ClS001', {'Tes Teori' => 74, 'Tes Praktek'=> 53, 'Presentasi'=> 86, 'Tes Wawancara'=> 88}],
      'Calas 2'=> ['CLS002', {'Tes Teori' => 73, 'Tes Praktek'=> 52, 'Presentasi'=> 85, 'Tes Wawancara'=> 87}],
      'Calas 3'=> ['CLS003', {'Tes Teori' => 67, 'Tes Praktek'=> 40, 'Presentasi'=> 85, 'Tes Wawancara'=> 85}],
      'Calas 4'=> ['CLS004', {'Tes Teori' => 62, 'Tes Praktek'=> 39, 'Presentasi'=> 84, 'Tes Wawancara'=> 82}],
      'Calas 5'=> ['CLS005', {'Tes Teori' => 57, 'Tes Praktek'=> 46, 'Presentasi'=> 60, 'Tes Wawancara'=> 75}],
      'Calas 6'=> ['CLS006', {'Tes Teori' => 57, 'Tes Praktek'=> 40, 'Presentasi'=> 82, 'Tes Wawancara'=> 84}],
      'Calas 7'=> ['CLS007', {'Tes Teori' => 59, 'Tes Praktek'=> 38, 'Presentasi'=> 70, 'Tes Wawancara'=> 75}],
      'Calas 8'=> ['CLS008', {'Tes Teori' => 57, 'Tes Praktek'=> 41, 'Presentasi'=> 82, 'Tes Wawancara'=> 82}],
    }
  end
end
