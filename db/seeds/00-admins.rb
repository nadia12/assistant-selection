module SeedAdmin
  def self.seed
    user = Admin.find_or_initialize_by(email: "admin@mail.com")
    user.password = 123123123
    user.password_confirmation = 123123123
    user.skip_confirmation!
    user.save

    user2 = Admin.find_or_initialize_by(email: "admin2@mail.com")
    user2.password = 123123123
    user2.password_confirmation = 123123123
    user2.skip_confirmation!
    user2.save
  end
end
