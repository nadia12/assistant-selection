module SeedCriterium
  CRITERIA = {'Tes Teori'=> 20, 'Tes Praktek'=>30, 'Presentasi'=>15, 'Tes Wawancara'=>35}
  def self.seed
    CRITERIA.each do |name, score|
      criterium = Criterium.find_or_initialize_by(name: name)
      criterium.score = score.to_f/100
      criterium.save
    end
  end
end
