class CreateProfiles < ActiveRecord::Migration
  def change
    create_table :profiles do |t|
      t.float  :gpa
      t.text   :address
      t.string :department
      t.string :faculty
      t.string :term
      t.string :class
      t.string :phone_number
      t.string :mobile_number
      t.date   :birthdate
      t.integer :assistant_candidate_id

      t.timestamps null: false
    end
  end
end
