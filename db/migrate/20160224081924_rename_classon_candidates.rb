class RenameClassonCandidates < ActiveRecord::Migration
  def change
    rename_column :profiles, :class, :class_name
  end
end
