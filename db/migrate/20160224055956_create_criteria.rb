class CreateCriteria < ActiveRecord::Migration
  def change
    create_table :criteria do |t|
      t.string  :name
      t.float   :score
      t.string  :code

      t.timestamps null: false
    end
  end
end
