class CreateAssistantCandidates < ActiveRecord::Migration
  def change
    create_table :assistant_candidates do |t|
      t.string :name
      t.float  :grade
      t.string :npm

      t.timestamps null: false
    end
  end
end
