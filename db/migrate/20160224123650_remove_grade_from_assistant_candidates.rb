class RemoveGradeFromAssistantCandidates < ActiveRecord::Migration
  def change
    remove_column :assistant_candidates, :grade, :float
  end
end
