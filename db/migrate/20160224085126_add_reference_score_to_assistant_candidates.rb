class AddReferenceScoreToAssistantCandidates < ActiveRecord::Migration
  def change
    add_column :assistant_candidates, :reference_score, :float
  end
end
