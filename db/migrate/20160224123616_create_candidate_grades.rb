class CreateCandidateGrades < ActiveRecord::Migration
  def change
    create_table :candidate_grades do |t|
      t.float   :grade
      t.integer :assistant_candidate_id
      t.integer :criterium_id

      t.timestamps null: false
    end
  end
end
