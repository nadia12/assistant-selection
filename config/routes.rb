Rails.application.routes.draw do
  devise_for :admins, controllers: { sessions: 'admin/sessions' }

  root to: "dashboard#index"
  resources :admins
  resources :candidates
  resources :criteria
  resources :selections
  get '/check', to: 'selections#check', as: 'check_process'
end
