class Ranking
  def initialize
    @optimum_alternatives = OptimumAlternative.new.process
    @candidate_rangkings ||= Hash.new
  end

  def process
    get_ranking_values
    update_reference_score
  end

  def get_ranking_values
    @optimum_alternatives.each do |candidate_name, alternative_grades|
      @candidate_rangkings[candidate_name] = alternative_grades.sum do |criteria_name, grade|
                                                grade.round(2) * criterium(criteria_name)
                                            end
    end
    return @candidate_rangkings
  end

  def update_reference_score
    @candidate_rangkings.each do |candidate_name, score|
      candidate = AssistantCandidate.find_by(name: candidate_name)
      candidate.reference_score  = score.round(2)
      candidate.save
    end
    return true
  end

  def criterium(name)
    Criterium.find_by(name: name).score.round(2)
  end

end
