class CriteriaSet
  def initialize
    @criteria_set ||= Hash.new
  end

  def process
    Criterium.all.each do |criterium|
      @criteria_set[criterium.name] = Hash.new
      (0..4).each do |counter|
        @criteria_set[criterium.name][counter] =  { bottom_limit: sets[counter][0],
                                                    upper_limit: sets[counter][1],
                                                    fuzzy_score:  fuzzy_scores[counter]}
      end
    end

    return @criteria_set
  end

  private
    def fuzzy_scores
      @fuzzy_scores ||=  [0, 0.25, 0.50, 0.75, 1.00]
    end

    def sets
      @sets ||= [ [0,20],
                  [20,40],
                  [40,60],
                  [60,80],
                  [80,100]
                ]
    end
end
