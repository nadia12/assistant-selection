class ValueMatching
  attr_accessor :criteria_sets

  def initialize(params={})
    @criteria_sets ||= CriteriaSet.new.process
    @matching_values ||= Hash.new
  end

  def process
    AssistantCandidate.all.each do |candidate|
      @matching_values[candidate.name] = Hash.new
      @criteria_sets.each do |criteria_name, sets|
        @matching_values[candidate.name][criteria_name] = matching(candidate.candidate_grades.value_by(criteria_name), sets)
      end
    end
    return @matching_values
  end

  private
  def matching(value, sets)
    sets.map.each do |key, set_values|
      if value > set_values[:bottom_limit] &&  value <= set_values[:upper_limit]
        return set_values[:fuzzy_score]
        break
      end
    end
  end

end
