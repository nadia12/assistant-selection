class OptimumAlternative
  attr_accessor :criteria_sets, :matching_values

  def initialize
    @process_matching_values ||= ValueMatching.new
    @matching_values ||=  @process_matching_values.process
    @criteria_sets ||= @process_matching_values.criteria_sets
    @optimum_alternatives ||= Hash.new
    @all_criteria_grades ||= Hash.new
   end

  def process
    @matching_values.each do |candidate_name, matchings|
      @optimum_alternatives[candidate_name] = Hash.new
      @criteria_sets.each do |criteria_name, sets|
        @optimum_alternatives[candidate_name][criteria_name] = rating_formula(matchings[criteria_name], all_criteria_grades[criteria_name])
      end
    end
    return @optimum_alternatives
  end

  private
    def rating_formula(grade, all_grades)
      grade = grade.to_f rescue 0
      result = grade / all_grades.max
      return result.round(2) rescue 0
    end

    def all_criteria_grades
      @matching_values.values.each do |val|
        val.each do |k,grade|
          @all_criteria_grades[k]  ||= Array.new
          grade = grade.round(2) rescue 0
          @all_criteria_grades[k] << grade
        end
      end
      return @all_criteria_grades
    end

end
