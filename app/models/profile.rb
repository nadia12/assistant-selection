# == Schema Information
#
# Table name: profiles
#
#  id                     :integer          not null, primary key
#  gpa                    :float
#  address                :text
#  department             :string
#  faculty                :string
#  term                   :string
#  class_name             :string
#  phone_number           :string
#  mobile_number          :string
#  birthdate              :date
#  assistant_candidate_id :integer
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#

class Profile < ActiveRecord::Base
end
