# == Schema Information
#
# Table name: criteria
#
#  id         :integer          not null, primary key
#  name       :string
#  score      :float
#  code       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Criterium < ActiveRecord::Base
  has_many :candidate_grades, dependent: :destroy
end
