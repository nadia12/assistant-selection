# == Schema Information
#
# Table name: candidate_grades
#
#  id                     :integer          not null, primary key
#  grade                  :float
#  assistant_candidate_id :integer
#  criterium_id           :integer
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#

class CandidateGrade < ActiveRecord::Base
  belongs_to :criterium
  belongs_to :assistant_candidate
  after_initialize :after_init

  scope :bonds, -> {eager_load(:criterium)}

  def self.value_by(criteria_name)
    bonds.where('criteria.name = ?', criteria_name).first.grade rescue 0
  end

  private
    def after_init
      self.grade = 0 if self.grade.blank?
    end
end
