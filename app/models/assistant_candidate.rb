# == Schema Information
#
# Table name: assistant_candidates
#
#  id              :integer          not null, primary key
#  name            :string
#  npm             :string
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  reference_score :float
#

class AssistantCandidate < ActiveRecord::Base
  has_one :profile, dependent: :destroy
  has_many :candidate_grades, dependent: :destroy
  accepts_nested_attributes_for :profile, reject_if: :all_blank, allow_destroy: true
  accepts_nested_attributes_for :candidate_grades, reject_if: :all_blank, allow_destroy: true

  after_initialize :after_init

  validates :name, :npm, presence: true
  validates  :npm, uniqueness: true

  def after_init
    self.profile = Profile.new if self.profile.blank?
  end

  def build_grade(criterium)
    self.candidate_grades.find_or_create_by(criterium_id: criterium.id)
  end

  def build_grades
    all_criteria ||= Criterium.all
    if self.candidate_grades.length < all_criteria.count
      all_criteria.each do |criterium|
        candidate_grades = self.candidate_grades
        candidate_grades.find_or_initialize_by(criterium_id: criterium.id)
      end
    end
  end
end
