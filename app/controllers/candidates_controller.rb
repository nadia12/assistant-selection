class CandidatesController < ApplicationController
  def index
    @candidates = AssistantCandidate.page(params[:page]).per(params[:per_page])
    @criteria ||= Criterium.all
  end

  def new
    @candidate = AssistantCandidate.new
    @candidate.build_grades
  end

  def create
    @candidate = AssistantCandidate.new(permitted_params)
    if @candidate.save
      redirect_to candidates_path, notice: 'Success save Candidate'
    else
      @candidate.build_grades
      flash[:errors] = @candidate.errors.full_messages
      render :new
    end
  end

  def edit
    resource.build_grades
    resource
  end

  def update
    if resource.update(permitted_params)
      redirect_to candidates_path, notice: 'Success updated Candidate'
    else
      flash[:errors] = resource.errors.full_messages
      render :edit
    end
  end

  def show
    resource
  end

  def destroy
    if resource.destroy
      flash[:notice] = 'Success delete Candidate'
    else
      flash[:errors] = 'Failed to delete Candidate'
    end
    redirect_to candidates_path
  end

  private

    def resource
      @candidate ||= AssistantCandidate.find params[:id]
    end

    def permitted_params
      params.require(:assistant_candidate).permit(:name,
                                        :grade,
                                        :npm,
                                        profile_attributes:
                                          [ :gpa,
                                            :phone_number,
                                            :mobile_number,
                                            :address,
                                            :department,
                                            :faculty,
                                            :term,
                                            :class_name,
                                            :birthdate
                                           ],
                                        candidate_grades_attributes:
                                        [ :id,
                                          :grade,
                                          :criterium_id
                                        ]
                                      )
    end
end
