class SelectionsController < ApplicationController
  def index
    @criteria = Criterium.all
    @candidates = AssistantCandidate.all.order(reference_score: :desc).page(params[:page]).per(params[:per_page])
  end

  def create
    @rangking = Ranking.new.process
    if @rangking
      redirect_to selections_path, notice: 'Success generate candidates'
    else
      flash[:errors] = 'Error Generate!'
      redirect_to :back
    end
  end

  def show
    resource
  end

  def check
    @criteria_sets = CriteriaSet.new.process
    @matching_values = ValueMatching.new
    @optimums = OptimumAlternative.new
    @rangkings = Ranking.new
  end

  private
  def resource
    @candidate ||= AssistantCandidate.find(params[:id])
  end
end
