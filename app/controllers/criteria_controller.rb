# == Schema Information
#
# Table name: criteria
#
#  id         :integer          not null, primary key
#  name       :string
#  score      :float
#  code       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class CriteriaController < ApplicationController
  def index
    @criteria = Criterium.page(params[:page]).per(params[:per_page])
  end

  def new
    @criterium = Criterium.new
  end

  def create
    @criterium = Criterium.new(permitted_params)
    score = params[:criterium][:score]
    @criterium.score = (score.to_f / 100) if score.present?
    if @criterium.save
      redirect_to criteria_path, notice: 'Success save Criteria'
    else
      flash[:errors] = @criterium.errors.full_messages
      render :new
    end
  end

  def edit
    resource
  end

  def update
    score = params[:criterium][:score]
    params[:criterium][:score] = (score.to_f / 100) if score.present?
    if resource.update(permitted_params)
      redirect_to criteria_path, notice: 'Success updated Criteria'
    else
      flash[:errors] = resource.errors.full_messages
      render :edit
    end
  end

  def show
    resource
  end

  def destroy
    if resource.destroy
      flash[:notice] = 'Success delete Criteria'
    else
      flash[:errors] = 'Failed to delete Criteria'
    end
    redirect_to criteria_path
  end

  private

    def resource
      @criterium ||= Criterium.find params[:id]
    end

    def permitted_params
      params.require(:criterium).permit(:name,
                                        :score,
                                        :code
                                      )
    end
end
