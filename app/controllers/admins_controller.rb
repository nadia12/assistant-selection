class AdminsController < ApplicationController
  def index
    @admins = Admin.all.page(params[:page]).per(params[:per_page])
  end
end
