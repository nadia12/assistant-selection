module ApplicationHelper
  def bootstrap_flash
    bootstrap_flash_for = { success: "alert-success", error: "alert-danger", errors: "alert-danger", alert: "alert-warning", notice: "alert-info" }
    flash.each do |msg_type, message|
      msg_type = msg_type.to_sym
      concat(content_tag(:div, message, class: "alert #{bootstrap_flash_for[msg_type]} alert-dismissible", role: 'alert') do
        concat(content_tag(:button, class: 'close', data: { dismiss: 'alert' }) do
          concat content_tag(:span, 'x')
          concat content_tag(:span, 'Close', class: 'sr-only')
        end)
        if message.class == Array
          message.each do |msg|
            concat content_tag(:div, msg)
          end
        else
          concat message
        end
      end)
    end
    nil
  end

  def active_class(controller_name)
    return 'active' if params[:controller] ==  controller_name
  end
end
